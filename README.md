# Backend Link Shortener

The backend project is to design, plan, and estimate building a simple link shortening service (think very simple bit.ly).

Estimations are we will have daily 1k~ active users with creating 5 to 10 short urls and the predicted daily volume is ~100k by clicking the links. This means that our system will have 10:1 read/write ratio. We will use base62 encoded value of a given unique id which will be short url itself. For example; if next id is 456 it resolve to something like `7m`. Additionally, if we don't use any expiration date to remove urls then using max length of 5 for a short url should be sufficient for even keeping all records during a year providing uniqueness. Here is a simple illlustration if we use base62 encoding with power to the n (max=5)

62^3 = 238,328<br>
62^4 = 14,776,336<br>
62^5 = 916,132,832<br>

### Architecture

![architecture](https://i.ibb.co/2NNLnX1/url-shortener.jpg)

### Database Choice
Since are no any transactional usage or restrictions I choose to go with DynamoDB as a NoSQL solution here. We can use `userId` as partition key and `shortUrl` (base62 encoded value from given unique Id) as range key. DynamoDB will provide scaling behind the scenes and it has consistent hashing which will provide same performance during its scaling up phase.

**urls** <br>
userId -> Partition key (Number) <br>
shortUrl -> Sort key (String) <br>
longUrl -> (String) <br>
creationDate -> (Date) <br>
expirationDate -> (Date) This is optional. <br>

**users** <br>
userId -> Partition key (Number) <br>
token -> (String) <br>
email -> (String) <br>
password -> (String) <br>

Besides these two main tables we also need to create an index to get a record with userId and longUrl to check if user is trying to create a short url from same long urls.

### Generating Sequential Id
We can benefit from Redis's counter which generates sequential id atomically. This id will be used to generate short url by encoding it into base62.

### Cache
We can also use Redis as distributed in-memory LRU cache so that we won't need to consume read capacity for dynamodb for hot partitions.

### Analytics
There are also various technologies to generate analytical reports like AWS Redshift for datawarehouse or using ElasticSearch. I believe ElasticSearch is a good option here since we are looking for daily analytics which we can create daily indices and compute analytics for bigger time intervals like weekly, monthly etc. There are two more components to feed ElasticSearch which are SQS and Lambda. Each read request either hitting the cache or dynamodb an event will created and send to SQS which will trigger a Lambda function to transform the event and persist it into ElasticSearch.

The simplest record to add in ElasticSearch can be like;

urlId: String <br>
clickedBy: <userId> - Number <br>
clickedDate: Date <br>

### SQS
SQS will be the buffer for keeping the generated read events (clicks) and will trigger Lambda.

### Lambda
Lambda function will get triggered by SQS whenever there are new records to process the event and persist them into ElasticSearch.

## REST Endpoints

### Create
**Descripton** : Create and get short url for given long url. <br>
**Type**: POST <br>

**Parameters**

| Field        | Param Type   | Type   | Description     |
| :----------- | :----------- | :----- | :-----          |
| id           | Path Param   | number   | Id of the user. |
| token        | Path Param   | string   | Token for authentication |

Steps for Creating a short url;
1. Check if current user created a short url with same long url. Return back the short url generated for same long url.
2. Get next sequential ID from Redis counter.
3. Encode id on base62 and persist new record in Dynamodb.
4. Return back to new short url created.


### Retrieve
**Descripton** : Retrieve the short url information <br>
**Type**: GET <br>

**Parameters**

| Field        | Param Type   | Type   | Description     |
| :----------- | :----------- | :----- | :-----          |
| id           | Path Param   | number   | Id of the user. |
| token        | Path Param   | string   | Token for authentication |
| shortUrl     | Path Param   | string   | Short url |

Steps for Retrieve a short url;
1. Check if cache has the requested short url. If it has then create a click event and send it into SQS. Return back the short url information
2. If cache doesn't has the requested url then get from Dynamodb and put it into cache. Create a click event and send it into SQS. Return back the short url information


### Delete
**Descripton** : Delete the short url information <br>
**Type**: DELETE <br>

**Parameters**

| Field        | Param Type   | Type   | Description     |
| :----------- | :----------- | :----- | :-----          |
| id           | Path Param   | number   | Id of the user. |
| token        | Path Param   | string   | Token for authentication |
| shortUrl     | Path Param   | string   | Short url |

Steps for Delete a short url;
1. Check if cache has the requested short url. If it has then delete the record from cache.
2. Delete the record from dynamodb.


### Update
**Descripton** : Update the short url information <br>
**Type**: PUT <br>

**Parameters**

| Field        | Param Type   | Type   | Description     |
| :----------- | :----------- | :----- | :-----          |
| id           | Path Param   | number   | Id of the user. |
| token        | Path Param   | string   | Token for authentication |
| shortUrl     | Path Param   | string   | Short url |
| updateFields | JSON body    | JSON     | Fields to update|

Steps for Update a short url;
1. Update the short url information in DynamoDB.
2. Update the short url information in cache if exists.

We might also have other fields than long url like; expirationDate, visibility etc.

### Task Breakdown

##### DynamoDB and Web-Service setup
This contains the work of creating DynamoDB tables and Web-Service with CRUD operations explained above for tables of URLs and Users. This also includes registering generating token for users.

### ElasticCache Setup
Create and configure a Redis cluster to get incremental ids. Once we configured the counter, we can integrate our web-service with redis to get sequential ids. Additionally, we will use Redis for caching so we can implement the logic for caching as well.

### Create ElasticSearch cluster
Create an ElasticSearch cluster for analyzing part of our application. We can test with some seed data to verify we created indices correctly and able to get basic queries.

### SQS and Lambda Setup
Create SQS queue with triggering Lambda option. Lambda function will get the message, transforms it if needed and persist into our ElasticSearch cluster.

### Endpoints for Reporting
Create and implement business logic for reporting. Required functionalities are;

1. Total number of clicks
2. Number of clicks by day
3. Total number of people who clicked links
4. Total number of people who clicked by day

### Monitoring and Alarming Setup
Create and configure CloudWatch to alarm and send notifications in case of bad events.


| Task Name    | Time in days |
| :----------- | :----------- |
| DynamoDB and Web-Service setup         | 3   |
| ElasticCache Setup        | 2   |
| Create ElasticSearch cluster     | 2 |
| SQS and Lambda Setup | 2 |
| Endpoints for Reporting | 2 |
| Testing and bug fixing | 2 |
| **Total** | **13** |

I added a half or a full day for buffer in case unexpected issues occuring. If I start today it should finished until 13rd of March with having 2 national holidays.